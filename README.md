# rebrain-devops-task1 

Task GIT 04 of module [DevOps -1] - Basic Git

This repo contains default config of [nginx server](https://nginx.org/)


![Nginx Logo](/images/nginx.png)

  
######  Example of Blockquotes:

Nobody:
> 
Bill Gates: 
> 640K of memory 
> should be enough for anybody


## Usage

1. clone this repository
1. copy ngnix.conf
1. edit nginx.conf
1. restart nginx service

## Installation

###### Requirements:
package | version
--------|-------
ngninx | 1.12.2
php-fpm | 7.0
wget | 1.14
curl | 7.29.0


###### Install this packages:
* nginx
* php-fpm
* wget
* curl

###### And then:
1. clone this repository `git clone git@gitlab.rebrainme.com:nur76n_at_mail_ru/rebrain-devops-task1.git`
1. `cd rebrain-devops-task1`  
1. `cp nginx.conf /etc/nginx`

###### Or in one line:
```
git clone git@gitlab.rebrainme.com:nur76n_at_mail_ru/rebrain-devops-task1.git && \
    cd rebrain-devops-task1 && \
    cp nginx.conf /etc/nginx
```

## License

MIT
